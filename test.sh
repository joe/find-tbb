#!/bin/sh

set -ex

CMAKE_MODULE_PATH=$(pwd)
mkdir "$1-build"
cd "$1-build"
cmake -DCMAKE_MODULE_PATH="$CMAKE_MODULE_PATH" $CMAKE_FLAGS ../"$1"
make
ctest -V
