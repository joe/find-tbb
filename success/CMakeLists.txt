cmake_minimum_required(VERSION 3.7)

enable_testing()

find_package(TBB)

add_executable(main main.cc)
target_link_libraries(main PUBLIC TBB::tbb)

add_test(NAME main COMMAND main)
