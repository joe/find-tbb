#include <tbb/tbb.h>
#include <numeric>

int main()
{
  int x[10] = {0};
  tbb::parallel_for(0,10,[&](int i){ x[i] = i; });
  return !(std::accumulate(x,x+10,0) == (9*10)/2);
}
