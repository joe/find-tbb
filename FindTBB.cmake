include(FindPackageHandleStandardArgs)

# Find TBB via a TBBConfig.cmake script
macro(_find_tbb_config)
  if(TBB_FIND_QUIETLY)
    set(_find_tbb_quiet_arg QUIET)
  else(TBB_FIND_QUIETLY)
    unset(_find_tbb_quiet_arg)
  endif(TBB_FIND_QUIETLY)

  if(TBB_FIND_VERSION_EXACT)
    set(_find_tbb_exact_arg EXACT)
  else(TBB_FIND_VERSION_EXACT)
    unset(_find_tbb_exact_arg)
  endif(TBB_FIND_VERSION_EXACT)

  set(_find_tbb_required_components)
  set(_find_tbb_optional_components)
  foreach(_find_tbb_comp IN LISTS TBB_FIND_COMPONENTS)
    if("${TBB_FIND_REQUIRED_${_find_tbb_comp}}")
      list(APPEND _find_tbb_required_components "${_find_tbb_comp}")
    else("${TBB_FIND_REQUIRED_${_find_tbb_comp}}")
      list(APPEND _find_tbb_optional_components "${_find_tbb_comp}")
    endif("${TBB_FIND_REQUIRED_${_find_tbb_comp}}")
  endforeach(_find_tbb_comp)

  find_package(TBB ${TBB_FIND_VERSION} ${_find_tbb_exact_arg}
    ${_find_tbb_quiet_arg}
    COMPONENTS ${_find_tbb_required_components}
    OPTIONAL_COMPONENTS ${_find_tbb_optional_components}
    CONFIG)
endmacro(_find_tbb_config)

# Find TBB via a tbbvars.sh script
macro(_find_tbb_tbbvars_sh)
endmacro(_find_tbb_tbbvars_sh)

# Find TBB integrated into the intel compiler
macro(_find_tbb_icc)
endmacro(_find_tbb_icc)

# Find TBB installed into the system
macro(_find_tbb_system)
endmacro(_find_tbb_system)

if("${TBB_DIR}")
  _find_tbb_config()
else("${TBB_DIR}")
  _find_tbb_config()
endif("${TBB_DIR}")

find_package_handle_standard_args(TBB
  HANDLE_COMPONENTS
  CONFIG_MODE)
